\contentsline {section}{\numberline {1}Ending of Renewing of this Lease Agreement}{3}% 
\contentsline {section}{\numberline {2}Charges and Payment}{3}% 
\contentsline {section}{\numberline {3}Additional Charges}{3}% 
\contentsline {section}{\numberline {4}Receipts}{4}% 
\contentsline {section}{\numberline {5}Security Deposit}{4}% 
\contentsline {section}{\numberline {6}Condition of Property}{4}% 
\contentsline {section}{\numberline {7}List of Existing Damages}{5}% 
\contentsline {section}{\numberline {8}Acceptance of Property}{5}% 
\contentsline {section}{\numberline {9}Rules of Dwelling}{5}% 
\contentsline {subsection}{\numberline {9.1}Kings Gate Articles of Incorporation}{5}% 
\contentsline {subsection}{\numberline {9.2}Authorized Occupants}{5}% 
\contentsline {subsection}{\numberline {9.3}Authorized Property Use}{6}% 
\contentsline {subsection}{\numberline {9.4}Command and Shared Areas}{6}% 
\contentsline {subsection}{\numberline {9.5}Maintenance}{6}% 
\contentsline {subsection}{\numberline {9.6}Utilities and Appliances}{6}% 
\contentsline {subsection}{\numberline {9.7}Smoke Detectors}{7}% 
\contentsline {subsection}{\numberline {9.8}Vehicle Parking}{7}% 
\contentsline {subsection}{\numberline {9.9}Miscellaneous Dwelling Rules}{7}% 
\contentsline {section}{\numberline {10}Property Owner Access to Premises}{8}% 
\contentsline {section}{\numberline {11}Retaliatory Eviction}{8}% 
\contentsline {section}{\numberline {12}Termination Hold-Over}{8}% 
\contentsline {section}{\numberline {13}Court Awarded Legal Fees}{8}% 
\contentsline {section}{\numberline {14}Move-Out Inspection/Surrender of Premises}{9}% 
\contentsline {section}{\numberline {15}Abandoned Property}{9}% 
\contentsline {section}{\numberline {16}Additional Correspondence}{9}% 
\contentsline {section}{\numberline {17}Additional Provisions}{10}% 
\contentsline {section}{\numberline {18}Governing Law}{11}% 
\contentsline {section}{\numberline {19}Agreement}{11}% 
